#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var shelljs_1 = require("shelljs");
var inquirer_1 = require("inquirer");
var chalk_1 = __importDefault(require("chalk"));
var figlet_1 = __importDefault(require("figlet"));
var conf_1 = __importDefault(require("conf"));
var commander_1 = __importDefault(require("commander"));
var questions_1 = __importDefault(require("./questions"));
var utils_1 = __importDefault(require("./utils"));
var fs_1 = __importDefault(require("fs"));
var composerConfig_1 = __importDefault(require("./composerConfig"));
var config = new conf_1.default({
    configName: "wp-install-tool"
});
var log = console.log;
log(figlet_1.default.textSync('WP Install Tool'));
commander_1.default
    .version('3.0.0')
    .description('WP Install Tool')
    .command('install')
    .action(function () {
    utils_1.default();
    inquirer_1.prompt(questions_1.default)
        .then(function (a) {
        shelljs_1.exec("wp core download --path=" + a.sitename + " --locale=" + a.lang);
        shelljs_1.cd(a.sitename);
        shelljs_1.exec("wp core config --dbname=\"" + (a.dbname ? a.dbname : a.sitename) + "\" --dbuser=\"" + (config.has('dbuser') ? config.get('dbuser') : a.dbuser) + "\" --dbpass=\"" + (config.has('dbpass') ? config.get('dbpass') : a.dbpassword) + "\" --dbhost=\"localhost\" --dbprefix=\"wp_\" --skip-check");
        // config.set('dbuser', a.dbuser)
        // config.set('dbpass', a.dbpassword)
        // log('💾 ' + chalk.green('Saved database config'))
        var install = shelljs_1.exec("wp core install --url=\"" + a.sitename + ".test\" --title=\"" + a.sitename + "\" --admin_user=\"" + a.username + "\" --admin_password=\"" + a.password + "\" --admin_email=\"" + a.email + "\"", { silent: true }, function (code, stdout, stderr) {
            if (stderr) {
                log('💥 ' + chalk_1.default.red(stderr));
                shelljs_1.cd('../');
                shelljs_1.rm('-rf', a.sitename);
                log('🧹 Cleaned up project directory');
            }
        });
        install.stdout.on('data', function (data) {
            shelljs_1.exec('wp option update permalink_structure "/%postname%/"');
            shelljs_1.exec('wp rewrite structure "/%postname%"');
            shelljs_1.exec('wp theme delete $(wp theme list --status=inactive --field=name)');
            shelljs_1.exec('wp plugin delete $(wp plugin list --status=inactive --field=name)');
            fs_1.default.writeFileSync('composer.json', composerConfig_1.default);
            shelljs_1.exec('composer install');
            if (a.installAio) {
                shelljs_1.exec('wp plugin install all-in-one-wp-migration --activate');
            }
            if (a.initGit) {
                shelljs_1.cd('wp-content');
                shelljs_1.exec("git init");
                shelljs_1.exec("git remote add origin " + a.gitOrigin);
                shelljs_1.cd('../');
            }
            shelljs_1.cd('../');
            log('🎉 ' + chalk_1.default.green('Your website is ready!'));
        });
    })
        .catch(function (e) {
        console.log(e);
    });
});
commander_1.default
    .command('clear')
    .action(function () {
    config.clear();
    log('🗑  ' + chalk_1.default.green('Configuration cleared'));
});
commander_1.default.parse(process.argv);
