"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var shelljs_1 = __importDefault(require("shelljs"));
/**
 * Check that required dependencies are installed
 */
function checkDeps() {
    if (!shelljs_1.default.which('wp') || !shelljs_1.default.which('git') || !shelljs_1.default.which('composer')) {
        console.log('⛔️ Please make sure WP-CLI, git and Composer are installed!');
        throw new Error('Missing dependencies');
    }
}
exports.default = checkDeps;
