declare const q: ({
    type: string;
    name: string;
    message: string;
    default: string;
    when?: undefined;
} | {
    type: string;
    name: string;
    message: string;
    default: boolean;
    when?: undefined;
} | {
    type: string;
    name: string;
    message: string;
    default: string;
    when: (a: any) => any;
})[];
export default q;
