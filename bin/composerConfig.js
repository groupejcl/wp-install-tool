"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config = "{\n    \"name\": \"districtweb/platform\",\n    \"type\": \"project\",\n    \"license\": \"MIT\",\n    \"repositories\":[\n        {\n            \"type\":\"composer\",\n            \"url\":\"https://wpackagist.org\"\n        }\n    ],\n    \"require\": {\n        \"php\": \"^7.2\",\n        \"roots/wp-password-bcrypt\": \"^1.0.0\"\n    },\n    \"config\": {\n        \"optimize-autoloader\": true,\n        \"preferred-install\": \"dist\",\n        \"sort-packages\": true\n    },\n    \"minimum-stability\": \"dev\",\n    \"prefer-stable\": true\n}\n";
exports.default = config;
