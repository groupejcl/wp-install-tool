const config: string = `{
    "name": "districtweb/platform",
    "type": "project",
    "license": "MIT",
    "repositories":[
        {
            "type":"composer",
            "url":"https://wpackagist.org"
        }
    ],
    "require": {
        "php": "^7.2",
        "roots/wp-password-bcrypt": "^1.0.0"
    },
    "config": {
        "optimize-autoloader": true,
        "preferred-install": "dist",
        "sort-packages": true
    },
    "minimum-stability": "dev",
    "prefer-stable": true
}
`

export default config