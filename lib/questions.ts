import Conf from 'conf'

const config = new Conf({
  configName: "wp-install-tool"
})

const q = [
  {
    type: 'input',
    name: 'sitename',
    message: 'Enter the site name',
    default: 'default'
  },
  {
    type: 'input',
    name: 'lang',
    message: 'Enter your desired language (default: fr_FR)',
    default: 'fr_FR'
  },
  {
    type: 'input',
    name: 'dbname',
    message: 'Enter the database name',
    default: 'default'
  },
  {
    type: 'input',
    name: 'dbuser',
    message: 'Enter the database user',
    default: 'root',
    when: function () {
      return !config.has('dbuser')
    }
  },
  {
    type: 'input',
    name: 'dbpassword',
    message: 'Enter the database password',
    default: 'secret',
    when: function () {
      return !config.has('dbpass')
    }
  },
  {
    type: 'input',
    name: 'username',
    message: 'Enter the website administrator\'s username',
    default: 'default'
  },
  {
    type: 'input',
    name: 'email',
    message: 'Enter the website administrator\'s email',
    default: 'default@example.org'
  },
  {
    type: 'input',
    name: 'password',
    message: 'Enter the website administrator\'s password',
    default: 'secret'
  },
  {
    type: 'confirm',
    name: 'installAio',
    message: 'Install All in One Migration plugin?',
    default: true
  },
  {
    type: 'confirm',
    name: 'initGit',
    message: 'Initialize a git repository?',
    default: false
  },
  {
    type: 'input',
    name: 'gitOrigin',
    message: 'Enter the repository origin url',
    default: '',
    when: function (a: any) {
      return a.initGit
    }
  }
]

export default q
