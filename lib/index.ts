#!/usr/bin/env node

import { exec, cd, rm } from 'shelljs'
import { prompt } from 'inquirer'
import chalk from 'chalk'
import figlet from 'figlet'
import Conf from 'conf'
import program from 'commander'
import q from './questions'
import checkDeps from './utils'
import fs from 'fs'
import composerConfig from './composerConfig'

const config = new Conf({
  configName: "wp-install-tool"
})

const log = console.log

log(figlet.textSync('WP Install Tool'))

program
  .version('3.0.0')
  .description('WP Install Tool')
  .command('install')
  .action(() => {
    checkDeps()
    prompt(q)
      .then((a) => {
        exec(`wp core download --path=${a.sitename} --locale=${a.lang}`)
        cd(a.sitename)
        exec(`wp core config --dbname="${a.dbname ? a.dbname : a.sitename}" --dbuser="${config.has('dbuser') ? config.get('dbuser') : a.dbuser}" --dbpass="${config.has('dbpass') ?config.get('dbpass') : a.dbpassword}" --dbhost="localhost" --dbprefix="wp_" --skip-check`)
        // config.set('dbuser', a.dbuser)
        // config.set('dbpass', a.dbpassword)
        
        // log('💾 ' + chalk.green('Saved database config'))

        let install = exec(
          `wp core install --url="${a.sitename}.test" --title="${a.sitename}" --admin_user="${a.username}" --admin_password="${a.password}" --admin_email="${a.email}"`,
          { silent: true }, function (code, stdout, stderr) {
            if (stderr) {
              log('💥 ' + chalk.red(stderr))
              cd('../')
              rm('-rf', a.sitename)
              log('🧹 Cleaned up project directory')
            }
          }
        )

        install.stdout.on('data', function (data) {
          exec('wp option update permalink_structure "/%postname%/"')
          exec('wp rewrite structure "/%postname%"')
          exec('wp theme delete $(wp theme list --status=inactive --field=name)')
          exec('wp plugin delete $(wp plugin list --status=inactive --field=name)')
          fs.writeFileSync('composer.json', composerConfig)
          exec('composer install')

          if (a.installAio) {
            exec('wp plugin install all-in-one-wp-migration --activate')
          }

          if (a.initGit) {
            cd('wp-content')
            exec(`git init`)
            exec(`git remote add origin ${a.gitOrigin}`)
            cd('../')
          }

          cd('../')
          log('🎉 ' + chalk.green('Your website is ready!'))
        })
      })
      .catch(e => {
        console.log(e)
      })
  })

program
  .command('clear')
  .action(() => {
    config.clear()
    log('🗑  ' + chalk.green('Configuration cleared'))
  })

program.parse(process.argv)
