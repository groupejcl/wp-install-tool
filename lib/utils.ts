import shell from 'shelljs'

/**
 * Check that required dependencies are installed
 */

export default function checkDeps () {
  if (!shell.which('wp') || !shell.which('git') || !shell.which('composer')) {
    console.log('⛔️ Please make sure WP-CLI, git and Composer are installed!')

    throw new Error('Missing dependencies')
  }
}
